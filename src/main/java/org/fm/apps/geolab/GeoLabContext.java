package org.fm.apps.geolab;

import org.fm.apps.geolab.commands.GeoLabCommand;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.apps.geolab.map.GeoLabMapWindow;

public class GeoLabContext {
    /* -------------------------------------------------------------------------
     * DISPLAYS
     ------------------------------------------------------------------------ */
    // list of datasets
    private Map<Integer,GeoLabMapWindow> windows = 
            new HashMap<Integer, GeoLabMapWindow>();

    public <T extends GeoLabMapWindow> T getWindow(int id) {
        return (T) (windows.containsKey(id) ? windows.get(id) : null);
    }
    
    public Set<Map.Entry<Integer, GeoLabMapWindow>>  windowsEntrySet() {
        return windows.entrySet();
    }
    
    public void removeWindow(int id) throws Exception {
        if(windows.remove(id) == null) {
            throw new Exception("Unknown window [" + id + "].");
        }
    }
    
    public <T extends GeoLabMapWindow> void addWindow(int id, T ds) throws Exception {
        if(windows.containsKey(id))  {
            throw new Exception("Window [" + id + "] is in use.");
        }
            
        windows.put(id, ds);
    }
    
    /* -------------------------------------------------------------------------
     * DATASETS
     ------------------------------------------------------------------------ */
    // list of datasets
    private Map<String,FmGeoDataset> datasets = 
            new HashMap<String, FmGeoDataset>();

    public <T extends FmGeoDataset> T getDataset(String name) {
        return (T) datasets.get(name);
    }
    
    public Set<Map.Entry<String, FmGeoDataset>>  datasetsEntrySet() {
        return datasets.entrySet();
    }
    
    public void removeDataset(String name) throws Exception {
        if(datasets.remove(name) == null) {
            throw new Exception("Unknown dataset [" + name + "].");
        }
    }
    
    public <T extends FmGeoDataset> void addDataset(String name, T ds) throws Exception {
        if(datasets.containsKey(name))  {
            throw new Exception("Name [" + name + "] is in use.");
        }
            
        datasets.put(name, ds);
    }
    
    
    /* -------------------------------------------------------------------------
     * COMMANDS
     ------------------------------------------------------------------------ */
    private final static  Map<String, Class<GeoLabCommand>> commands = 
            new HashMap<String, Class<GeoLabCommand>>();

    public static <T extends GeoLabCommand> void addCommand(String[] cmdstr, Class<T> cmd) {
        for (String s : cmdstr) {
            commands.put(s, (Class<GeoLabCommand>) cmd);
        }
    }

    public static Class<GeoLabCommand> findCommand(String cmdstr) {
        return commands.get(cmdstr);
    }
    //
    private final static Map<String, GeoLabCommand> commandBeans = new HashMap<String, GeoLabCommand>();

    public GeoLabCommand instance(String name) {
        try {
            if (commandBeans.containsKey(name)) {
                return commandBeans.get(name);
            }
            Class<GeoLabCommand> cmdcls = commands.get(name);
            if (cmdcls == null) {
                return null;
            }

            GeoLabCommand cmd = cmdcls.newInstance();
            commandBeans.put(name, cmd);

            return cmd;
        } catch (Exception e) {
            return null;
        }

    }
}
