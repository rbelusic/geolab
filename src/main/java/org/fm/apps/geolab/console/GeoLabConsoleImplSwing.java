package org.fm.apps.geolab.console;

import org.fm.application.FmContext;
import org.fm.dm.DmFactory;
import org.fm.ml.hosts.MlHost;

/**
 *
 * @author rbelusic
 */
public class GeoLabConsoleImplSwing extends javax.swing.JFrame implements GeoLabConsole {
    private MlHost host;

    /**
     * Creates new form GeoLabConsole
     */
    public GeoLabConsoleImplSwing() {
        super();
        host = new MlHost(FmContext.getApplication(), DmFactory.create(), this);
        host.addListener(this);
        host.run();

        initComponents();
        
        appConsole.init();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        appConsole = new org.fm.impl.swing.ml.observers.MljConsole();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GeoLab 0.1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(appConsole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(appConsole, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.fm.impl.swing.ml.observers.MljConsole appConsole;
    // End of variables declaration//GEN-END:variables

    @Override
    public MlHost getHost() {
        return host;
    }
    
    public void out(String s) {
    }
    
    public void err(String s) {
    }

    public void println() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
