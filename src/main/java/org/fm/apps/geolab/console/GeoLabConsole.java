package org.fm.apps.geolab.console;

import org.fm.impl.swing.ml.MlHostSwingComponent;
import org.fm.ml.hosts.MlHost;

/**
 *
 * @author RobertoB
 */
public interface GeoLabConsole extends MlHostSwingComponent {

public void println();
}
