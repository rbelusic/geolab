package org.fm.apps.geolab.commands;

import java.util.List;
import org.fm.apps.geolab.GeoLabContext;

public interface GeoLabCommand {
    public void execute(String cmd, GeoLabContext ctx, List<String> args) throws Exception;
}
