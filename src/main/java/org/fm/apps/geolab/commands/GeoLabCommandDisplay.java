/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.apps.geolab.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.impl.swing.map.FmMapDisplayDriverSwing;
import org.fm.apps.geolab.GeoLabContext;
import org.fm.apps.geolab.map.GeoLabMapWindow;

/**
 *
 * @author rbelusic
 */
public class GeoLabCommandDisplay implements GeoLabCommand {

    public static final String COMMANDS[] = {"DISPLAY"};
    // list of dataset classes
    private final static Map<String, Class> displayDrivers =
            new HashMap<String, Class>();

    static {
        displayDrivers.put("GUI", FmMapDisplayDriverSwing.class);
        // displayClasses.put("PRINT",FmMapDisplayPrint.class);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public void execute(String cmd, GeoLabContext ctx, List<String> args) throws Exception {
        if (cmd.equalsIgnoreCase("DISPLAY")) {
            cmd = args.remove(0);
            if (cmd.equalsIgnoreCase("OPEN")) {
                _open(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("CLOSE")) {
                _close(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("LIST")) {
                _list(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("SHOW")) {
                _show(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("HIDE")) {
                _hide(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("RESIZE")) {
                _resize(ctx, args);
                return;
            }

            throw new Exception("Syntax error");
        }
    }

    private void _close(GeoLabContext ctx, List<String> args) throws Exception {
        // DISPLAY CLOSE [id]
        int id = Integer.parseInt(args.remove(0));
        GeoLabMapWindow window = ctx.getWindow(id);

        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }
        window.close();
        ctx.removeWindow(id);
        System.out.println("Window [" + id + "] is closed.");
    }

    private void _open(GeoLabContext ctx, List<String> args) throws Exception {
        // DISPLAY OPEN [id] [x] [y] [w] [h]
        int id = Integer.parseInt(args.remove(0));

        if (ctx.getWindow(id) != null) {
            throw new Exception("Display [" + id + "] is in use.");
        }

        GeoLabMapWindow window = new GeoLabMapWindow(id);
        ctx.addWindow(id, window);
        window.setVisible(true);

        System.out.println("Display [" + id + "] is opened.");
    }

    private void _list(GeoLabContext ctx, List<String> args) {
        System.out.println("List of windows:");
        System.out.printf("%3s %60s \n", "ID", "TITLE");
        System.out.printf("===================================================\n");
        for (Map.Entry<Integer, GeoLabMapWindow> e : ctx.windowsEntrySet()) {
            System.out.printf("%03d %60s \n", e.getKey(), e.getValue().getTitle());
        }
    }

    private void _describe(GeoLabContext ctx, List<String> args) throws Exception {
        String name = args.remove(0);
        FmGeoDataset ds = ctx.getDataset(name);
        if (ds == null) {
            throw new Exception("Dataset [" + name + "] is not loaded.");
        }

        System.out.println("Dataset [" + name + "]:");
        System.out.printf("Dataset [%s], type  %s\n", name, ds.getType());
        for (Map.Entry<String, Object> attr : ds.getInfo().getAttr().entrySet()) {
            System.out.printf("%20s: %s\n", attr.getKey(), attr.getValue());
        }
    }

    private void _show(GeoLabContext ctx, List<String> args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void _hide(GeoLabContext ctx, List<String> args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void _resize(GeoLabContext ctx, List<String> args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
