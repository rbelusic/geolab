package org.fm.apps.geolab.commands;

import java.util.List;
import java.util.Map;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.geo.model.FmRangeLutTable;
import org.fm.geo.model.FmRgb;
import org.fm.map.layer.impl.FmMapLayerRaster;
import org.fm.map.layer.impl.FmMapLayerVector;
import org.fm.map.layer.impl.FmMapLayerWms;
import org.fm.map.layer.model.FmMapLayer;
import org.fm.map.layer.model.FmMapLayerOptions;
import org.fm.map.layer.model.FmMapLayerRasterOptions;
import org.fm.map.layer.model.FmMapLayerVectorOptions;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapSymbolset;
import org.fm.map.model.FmPalTable;
import org.fm.map.model.FmSimplePalTable;
import org.fm.apps.geolab.GeoLabContext;
import org.fm.apps.geolab.map.GeoLabMapWindow;

/**
 *
 * @author rbelusic
 */
public class GeoLabCommandLayer implements GeoLabCommand {

    public static final String COMMANDS[] = {"LAYER"};

    public String[] getCommands() {
        return COMMANDS;
    }

    public void execute(String cmd, GeoLabContext ctx, List<String> args) throws Exception {
        if (cmd.equalsIgnoreCase("LAYER")) {
            cmd = args.remove(0);
            if (cmd.equalsIgnoreCase("ADD")) {
                _open(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("REMOVE")) {
                _close(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("LIST")) {
                _list(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("DESCRIBE")) {
                _describe(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("SHOW")) {
                _show(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("HIDE")) {
                _hide(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("PAL")) {
                _pal(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("LUT")) {
                _lut(ctx, args);
                return;
            } else if (cmd.equalsIgnoreCase("SYM")) {
                _sym(ctx, args);
                return;
            }
        }

        throw new Exception("Syntax error");
    }

    private void _list(GeoLabContext ctx, List<String> args) {
        System.out.println("List of windows:");
        System.out.printf("%3s %60s \n", "ID", "TITLE");
        System.out.printf("===================================================\n");
        for (Map.Entry<Integer, GeoLabMapWindow> e : ctx.windowsEntrySet()) {
            System.out.printf("%03d %60s \n", e.getKey(), e.getValue().getTitle());
        }
    }

    private void _describe(GeoLabContext ctx, List<String> args) throws Exception {
        String name = args.remove(0);
        FmGeoDataset ds = ctx.getDataset(name);
        if (ds == null) {
            throw new Exception("Dataset [" + name + "] is not loaded.");
        }

        System.out.println("Dataset [" + name + "]:");
        System.out.printf("Dataset [%s], type  %s\n", name, ds.getType());
        for (Map.Entry<String, Object> attr : ds.getInfo().getAttr().entrySet()) {
            System.out.printf("%20s: %s\n", attr.getKey(), attr.getValue());
        }
    }

    private void _show(GeoLabContext ctx, List<String> args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void _hide(GeoLabContext ctx, List<String> args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void _open(GeoLabContext ctx, List<String> args) throws Exception {
        /*
         * LAYER ADD FOR RASTER DATASET:
         *  LAYER ADD  [win] [name] [dataset] {band|0} {lut|*} 
         *             {pal|*} {nodataPalIndex|nodataColor|*} {fgPalIndex|fgColor} 
         * 
         * LAYER ADD FOR VECTOR DATASET:
         *  LAYER ADD [win] [name] [dataset] {lut|*} 
         *            {sym|*} {defaultSymIndex} 
         * 
         * LAYER ADD FOR WMS DATASET:
         *  LAYER ADD [win] [name] [wms:url] [wmsLayer]
         */
        
        int id = Integer.parseInt(args.remove(0));
        String name = args.remove(0);
        String dsname = args.remove(0);
        GeoLabMapWindow window = ctx.getWindow(id);
        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }
        FmMapDisplay map = window.getMapDisplay();

        if (dsname.toLowerCase().startsWith("wms:")) {
            FmMapLayer layer = new FmMapLayerWms();
            FmMapLayerRasterOptions layeropts = new FmMapLayerRasterOptions();
            layeropts.datesetUrl = dsname.substring(4);
            layeropts.datesetLayer = args.remove(0);
            layer.init(layeropts);
            map.addLayer(layer);
            System.out.println("WMS layer [" + layeropts.datesetLayer + "] added to display [" + id + "] as layer.");
        } else {
            FmGeoDataset ds = ctx.getDataset(dsname);
            if (ds == null) {
                throw new Exception("Unknown dataset [" + dsname + "].");
            }
            FmMapLayer layer = ds.getType().equals(FmGeoDataset.Type.RASTER)
                    ? new FmMapLayerRaster() : new FmMapLayerVector();
            FmMapLayerOptions layeropts;
            // raster
            if (ds.getType().equals(FmGeoDataset.Type.RASTER)) {
                layeropts = new FmMapLayerRasterOptions();
                String bandStr = args.size() > 0 ? args.remove(0) : "0";  
                ((FmMapLayerRasterOptions) layeropts).band = Integer.parseInt(bandStr);  
                
                String lutName = args.size() > 0 ? args.remove(0).trim() : "*";                
                layeropts.lut = lutName.equals("*") ? null : FmRangeLutTable.instance(lutName);
                
                String palName = args.size() > 0 ? args.remove(0).trim() : "*";
                ((FmMapLayerRasterOptions) layeropts).pal = 
                        palName.equals("*") ? null : FmSimplePalTable.instance(palName);
                
                String fgDef = "0:0:0:255";
                String ndDef = "200:200:200:255";
                
                // nodata
                String ndStr = args.size() > 0 ? args.remove(0).trim() : ndDef;
                if(ndStr.equals("*")) {
                    ndStr = ndDef;
                }
                ((FmMapLayerRasterOptions) layeropts).nodataColor =
                        ndStr.indexOf(":") > -1 ? FmRgb.RGBA(ndStr)
                        : (
                        ((FmMapLayerRasterOptions) layeropts).pal != null ? 
                            FmRgb.RGBA(ndDef)
                        : ((FmMapLayerRasterOptions)layeropts).pal.mapValue(Integer.parseInt(ndStr))
                        );
                
                // fg 
                String fgStr = args.size() > 0 ? args.remove(0).trim() : fgDef;
                if(fgStr.equals("*")) {
                    fgStr = fgDef;
                }
                ((FmMapLayerRasterOptions) layeropts).foregroundColor =
                        fgStr.indexOf(":") > -1 ? FmRgb.RGBA(fgStr)
                        : (
                        ((FmMapLayerRasterOptions) layeropts).pal != null ? 
                            FmRgb.RGBA(fgDef)
                        : ((FmMapLayerRasterOptions)layeropts).pal.mapValue(Integer.parseInt(fgStr))
                        );
            } else {
                // vecor
                layeropts = new FmMapLayerVectorOptions();
                String lutName = args.size() > 0 ? args.remove(0).trim() : "*";                
                layeropts.lut = lutName.equals("*") ? null : FmRangeLutTable.instance(lutName);                
                
                String symName = args.size() > 0 ? args.remove(0).trim() : "*";
                ((FmMapLayerVectorOptions) layeropts).sym = 
                        symName.equals("*") ? null : FmMapSymbolset.instance(symName);                                

                
                // fg 
                String fgStr = args.size() > 0 ? args.remove(0).trim() : "*";
                ((FmMapLayerVectorOptions) layeropts).foregroundSymbol =
                    fgStr.equals("*") || ((FmMapLayerVectorOptions) layeropts).sym == null ? 
                        null : (
                        ((FmMapLayerVectorOptions)layeropts).sym.getSymbol(Integer.parseInt(fgStr))                            
                    );
            }
            
            // common
            layeropts.dataset = ds;
            layer.setName(name);
            layer.init(layeropts);
            map.addLayer(layer);
            System.out.println("Dataset [" + dsname + "] added to display [" + id + "] as layer [" + name + "].");
        }
    }

    private void _close(GeoLabContext ctx, List<String> args) throws Exception {
        // LAYERS CLOSE >> [win] [name]
        int id = Integer.parseInt(args.remove(0));
        String name = args.remove(0);
        GeoLabMapWindow window = ctx.getWindow(id);
        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }

        FmMapDisplay map = window.getMapDisplay();
        FmMapLayer l = map.getLayer(name);
        if (l == null) {
            throw new Exception("Layer with name [" + name + "] is not found on display [" + id + "].");
        }

        map.removeLayer(l);
        l.dispose();
        System.out.println("Layer [" + name + "] is removed from display [" + id + "].");
    }

    private void _lut(GeoLabContext ctx, List<String> args) throws Exception {
        // LAYERS LUT >> [win] [name] [lutUrl]
        int id = Integer.parseInt(args.remove(0));
        String name = args.remove(0);
        String ssname = args.remove(0);
        GeoLabMapWindow window = ctx.getWindow(id);
        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }
        FmMapDisplay map = window.getMapDisplay();
        FmMapLayer l = map.getLayer(name);
        if (l == null) {
            throw new Exception("Layer with name [" + name + "] is not found on display [" + id + "].");
        }
        if (!(l instanceof FmMapLayerVector)) {
            ((FmMapLayerVector)l).setLutTable(FmRangeLutTable.instance(ssname));
        } else if (!(l instanceof FmMapLayerRaster)) {
            ((FmMapLayerRaster)l).setLutTable(FmRangeLutTable.instance(ssname));
        }
        
        
        System.out.println("Done.");
        map.redraw(null);
    }

    private void _sym(GeoLabContext ctx, List<String> args) throws Exception {
        // LAYERS SYM >> [win] [name] [symbolssetUrl]
        int id = Integer.parseInt(args.remove(0));
        String name = args.remove(0);
        String ssname = args.remove(0);
        GeoLabMapWindow window = ctx.getWindow(id);
        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }
        FmMapDisplay map = window.getMapDisplay();
        FmMapLayer l = map.getLayer(name);
        if (l == null) {
            throw new Exception("Layer with name [" + name + "] is not found on display [" + id + "].");
        }
        if (!(l instanceof FmMapLayerVector)) {
            throw new Exception("Layer [" + name + "] is not of vector type.");
        }
        ((FmMapLayerVector)l).setSymbolset(new FmMapSymbolset(ssname));
        
        System.out.println("Done.");
        map.redraw(null);
    }

    private void _pal(GeoLabContext ctx, List<String> args) throws Exception {
        // LAYERS PAL >> [win] [name] [palURI]
        int id = Integer.parseInt(args.remove(0));
        String name = args.remove(0);
        String filename = args.remove(0);
        
        GeoLabMapWindow window = ctx.getWindow(id);
        if (window == null) {
            throw new Exception("Display [" + id + "] is not in use.");
        }

        FmMapDisplay map = window.getMapDisplay();
        FmMapLayer l = map.getLayer(name);
        if (l == null) {
            throw new Exception("Layer with name [" + name + "] is not found on display [" + id + "].");
        }
        FmMapLayerOptions opts = l.getOptions();
        if(opts.dataset == null || !opts.dataset.getType().equals(FmGeoDataset.Type.RASTER)) {
            throw new Exception("Layer with name [" + name + "] is not RASTER layer.");
        }
        FmPalTable pal = FmSimplePalTable.instance(filename);
        ((FmMapLayerRaster)l).setPalette(pal);
        System.out.println("Layer [" + name + "] palette changed.");
    }

}
