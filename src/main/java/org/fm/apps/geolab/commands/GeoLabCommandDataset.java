/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.apps.geolab.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.geo.dataset.impl.FmGeoDatasetDem;
import org.fm.geo.dataset.impl.FmGeoDatasetShapefile;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.geo.dataset.model.FmGeoDatasetOptions;
import org.fm.geo.dataset.model.FmGeoDatasetRasterOptions;
import org.fm.geo.dataset.model.FmGeoDatasetVectorOptions;
import org.fm.apps.geolab.GeoLabContext;

/**
 *
 * @author rbelusic
 */
public class GeoLabCommandDataset implements GeoLabCommand {
    public static final String COMMANDS[] = {"DATASET"};
    
    // list of dataset classes
    private final static Map<String,Class> datasetClasses = 
            new HashMap<String, Class>();
    
 
    static {
        datasetClasses.put("DEM",FmGeoDatasetDem.class);
        datasetClasses.put("SHAPEFILE", FmGeoDatasetShapefile.class);
    }

    
    public String [] getCommands() {
        return COMMANDS;
    }
    
    public void execute(String cmd, GeoLabContext ctx, List<String> args) throws Exception {
        if(cmd.equalsIgnoreCase("DATASET")) {
            cmd = args.remove(0);
            if(cmd.equalsIgnoreCase("OPEN")) {
                _open(ctx, args);
                return;
            } else if(cmd.equalsIgnoreCase("CLOSE")) {
                _close(ctx, args);
                return;
            } else if(cmd.equalsIgnoreCase("LIST")) {
                _list(ctx, args);
                return;
            } else if(cmd.equalsIgnoreCase("DESCRIBE")) {
                _describe(ctx, args);
                return;
            }
        }
        
        throw new Exception("Syntax error");
    }

    private void _close(GeoLabContext ctx, List<String> args) throws Exception {
        // DATASET CLOSE [name]
        String name = args.remove(0);
        FmGeoDataset ds = ctx.getDataset(name);

        if(ds == null) {
            throw new Exception("Unknown dataset [" + name + "].");
        }
        
        ds.close();
        ctx.removeDataset(name);
        System.out.println("Dataset [" + name + "] is closed.");
        
    }
    
    private void _open(GeoLabContext ctx, List<String> args) throws Exception {
        // DATASET OPEN [name] [type] [uri] [mode]
        // dataset open dem0 dem c:/temp/test.dem" rw
        // dataset open demWgs wgs http://somehost.com/wgs" rw
        // dataset open rivers shapefile c:/temp/test.shp" r
        // find type
        
        String name = args.remove(0);
        if(ctx.getDataset(name) != null) {
            throw new Exception("Name [" + name + "] is in use.");
        }
        
        Class<FmGeoDataset> dsClass = datasetClasses.get(args.get(0).toUpperCase());
        FmGeoDataset ds = dsClass.newInstance();
        FmGeoDatasetOptions opts;
        if(ds.getType().equals(FmGeoDataset.Type.RASTER)) {
            FmGeoDatasetRasterOptions optsR = new FmGeoDatasetRasterOptions();
            opts = new FmGeoDatasetRasterOptions();
        } else if(ds.getType().equals(FmGeoDataset.Type.VECTOR)) {
            opts = new FmGeoDatasetVectorOptions();
        } else {
            throw new Exception("Unknown data type.");
        }
        opts.fileName = args.get(1);
        opts.openMode = FmGeoDataset.OpenMode.valueOf(args.get(2).toUpperCase());

        ds.open(opts);
        ctx.addDataset(name, ds);

        System.out.println("Dataset [" + name + "] is opened.");
    }

    private void _list(GeoLabContext ctx, List<String> args) {
        System.out.println("List of loaded datasets:");
        System.out.printf("%30s %10s \n", "NAME", "TYPE");
        System.out.printf("===================================================\n");
        for(Map.Entry<String, FmGeoDataset> e: ctx.datasetsEntrySet()) {
            System.out.printf("%30s %10s \n", e.getKey(), e.getValue().getType());
        }
        
    }

    private void _describe(GeoLabContext ctx, List<String> args) throws Exception {
        String name = args.remove(0);
        FmGeoDataset ds = ctx.getDataset(name);
        if(ds == null) {
            throw new Exception("Dataset [" + name + "] is not loaded.");
        }        
        
        System.out.println("Dataset [" + name + "]:");
        System.out.printf("Dataset [%s], type  %s\n", name, ds.getType());
        for(Map.Entry<String, Object> attr: ds.getInfo().getAttr().entrySet()) {
            System.out.printf("%20s: %s\n", attr.getKey(), attr.getValue());
        }
    }
}
