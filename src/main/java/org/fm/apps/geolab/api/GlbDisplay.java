package org.fm.apps.geolab.api;

import java.util.HashMap;
import java.util.Map;
import org.fm.apps.geolab.map.GeoLabMapWindow;

public class GlbDisplay {

    private  Map<Integer, GeoLabMapWindow> displays =
            new HashMap<Integer, GeoLabMapWindow>();

    public <T extends GeoLabMapWindow> void add(int id, Class<T> wCls) throws Exception {        
        if (displays.containsKey(id)) {
            throw new Exception("Display [" + id + "] is in use.");
        }

        T window = wCls.newInstance();
        window.setDisplayIndex(id);
        displays.put(id, window);
        window.setVisible(true);
    }
    
    public void remove(int id) throws Exception {
        GeoLabMapWindow window;
        
        if((window = displays.remove(id)) == null) {
            throw new Exception("Unknown window [" + id + "].");
        }
        
        window.close();
    }

    public <T extends GeoLabMapWindow> T get(int id) {
        return (T) displays.get(id);
    }
    
    public <T extends GeoLabMapWindow> Map<Integer,T> getDisplays() {
        return (Map<Integer, T>) displays;
    }

    
    public void clear() {
        Map<Integer, GeoLabMapWindow> oldDisplays = displays;
        displays = new HashMap<Integer, GeoLabMapWindow>();
        
        for(Map.Entry<Integer, GeoLabMapWindow> e: oldDisplays.entrySet()) {
            e.getValue().close();
        }
    }

}
