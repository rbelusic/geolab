package org.fm.apps.geolab.map;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RobertoB
 */
public class RefreshGui extends javax.swing.JFrame {
    public final static long GUI_REFRESH_TIMEOUT = 800L;    
    private int refreshRequestCount = 0;

    class RefreshTimer extends Timer {
        private long scheduleTime;
        private Runnable task;
        private TimerTask timerTask;

        public void schedule(Runnable runnable, long delay) {            
            task = runnable;
            timerTask = new TimerTask() {
                public void run() {
                    task.run();
                }
            };
            scheduleTime = System.currentTimeMillis() + delay;
            schedule(timerTask, delay);
        }

        public void reschedule(long delay) {
            timerTask.cancel();
            timerTask = new TimerTask() {
                public void run() {
                    task.run();
                }
            };
            scheduleTime = System.currentTimeMillis() + delay;
            schedule(timerTask, delay);
        }
        
        public long getScheduleTime() {
            return scheduleTime;
        }
    }

    RefreshTimer refreshTimer = null;

    /**
     * Creates new form RefreshGui
     */
    public RefreshGui() {
        initComponents();

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("MOUSE+");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("MOUSE-");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                // This is only called when the user releases the mouse button.
                refreshPanel();
            }
        });

    }

    private synchronized void refreshPanel() {
        refreshRequestCount++;
        if (refreshTimer == null) {
            System.out.println(">> Timer created.");
            refreshTimer = new RefreshTimer();
            refreshTimer.schedule(
                    new Runnable() {
                        @Override
                        public void run() {
                            // moramo li osvjeziti ekran
                            if (refreshRequestCount > 0) {
                                System.out.println("\n*** START Redraw ***:" + refreshRequestCount);
                                refreshRequestCount = 0;
                                try {
                                    Thread.currentThread().sleep(3000L);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(RefreshGui.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                System.out.println("*** END Redraw ***:" + refreshRequestCount);
                                refreshTimer.reschedule(2000L);
                            } else {
                                System.out.println("<< Reset timer.");
                                refreshTimer = null;
                            }
                        }
                     ;
                },
                GUI_REFRESH_TIMEOUT
            );
        } else {
            System.out.print("|" + refreshRequestCount);
            refreshTimer.reschedule(GUI_REFRESH_TIMEOUT);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        pnlRefresh = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout pnlRefreshLayout = new javax.swing.GroupLayout(pnlRefresh);
        pnlRefresh.setLayout(pnlRefreshLayout);
        pnlRefreshLayout.setHorizontalGroup(
            pnlRefreshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        pnlRefreshLayout.setVerticalGroup(
            pnlRefreshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RefreshGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RefreshGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RefreshGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RefreshGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RefreshGui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JPanel pnlRefresh;
    // End of variables declaration                   
}