package org.fm.apps.geolab;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.apps.geolab.commands.GeoLabCommand;
import org.fm.apps.geolab.commands.GeoLabCommandDataset;
import org.fm.apps.geolab.commands.GeoLabCommandDisplay;
import org.fm.apps.geolab.commands.GeoLabCommandLayer;
import org.fm.apps.geolab.console.GeoLabConsoleImplSwing;
import org.fm.apps.geolab.map.GeoLabMapWindow;

public class GeoLab extends FmApplication {
    Pattern cmdPattern = Pattern.compile("([^\"]\\S*|\".+?\")\\s*");
    GeoLabConsoleImplSwing console = new GeoLabConsoleImplSwing();
    GeoLabContext ctx = new GeoLabContext();

    public GeoLab() {
        _configure();
    }

    private void _configure() {
        FmContext.setApplication(this);
        
        // add commands 
        GeoLabContext.addCommand(GeoLabCommandDataset.COMMANDS, GeoLabCommandDataset.class);
        GeoLabContext.addCommand(GeoLabCommandDisplay.COMMANDS, GeoLabCommandDisplay.class);
        GeoLabContext.addCommand(GeoLabCommandLayer.COMMANDS, GeoLabCommandLayer.class);

        // shos console
        console.getHost().addListener(this);
        console.setVisible(true);
    }
  
    public void onConsoleInput(Object sender, Object data) {
        String exprStr = data.toString();
        List<String> list = new ArrayList<String>();
 
        // tokenize
        Matcher m = cmdPattern.matcher(exprStr);
        while (m.find()) {
            list.add(m.group(1).replace("\"", ""));
        }
        if (list.isEmpty()) {
            return;
        }

        String cmd = list.get(0).toUpperCase();
        list.remove(0);

        // exec
        try {
            _exec(cmd, list);
        } catch (Exception e) {
            System.err.print("Error occured: \n " + e.getMessage());
        }
    }


    public void onCloseMapWindow(Object sender, Object data) throws Exception {
        Integer winIndex = (Integer)data;
        GeoLabMapWindow win = ctx.getWindow(winIndex);
        win.close();
        ctx.removeWindow(winIndex);
    }
    
    private void _exec(String cmdstr, List<String> list) throws Exception {
        GeoLabCommand cmd = ctx.instance(cmdstr);
        if (cmd == null) {
            throw new Exception("Command not found");
        }

        System.out.println("Executing " + cmd + " ...");
        cmd.execute(cmdstr, ctx, list);
        System.out.println("\nDone.");
    }


    public static void main(String[] args) {
        new GeoLab();
    }
}
